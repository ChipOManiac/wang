#!/usr/bin/env bash

# wang - a script to set pre-defined backgrounds on X11
# setbg was the previous name for this script. This script is (I think)
# optimal for use with the xloadimage, xsetbg, and xview set of programs
# but can be used with other programs (like feh).
#
# This script assumes you have some variant of sed, grep, and cut.

# > synopsis
# wang [ -r ] [ -S ] [ -t <tag> ] [ -d ] [ -o <number> ] [ -s | -l | -p ] [ -f <final name> ]
# 
# -r  Randomize selection (mutually exclusive with -f)
# -S  Use only safe options
# -t  Select only from <tag>
# -d  Use a default from the filter
# -o  Use option number <number> instead
# -s  Set the background to this final (uses the first if the results
#     are more than one)
# -l  Lists all finals that satisfy the criteria
# -p  Print safety information about the current final
# -n  Use this specific final
#
# If no arguments or options are given, wang assumes you want to use
# the default final.

# > variables
bg_file_path="${HOME}/.backgrounds"

#   pull file
bg_input=$(< $bg_file_path)

#   pull section
cmd_root=$( sed 1q <<< "$bg_input" );
sec_replaces=$( sed -n '/^R/{ s/\t/ /g; s/  / /g; s/^..//; p }' <<< "$bg_input" )
sec_options=$( sed -n '/^O/{ s/^..//; p }' <<< "$bg_input" )
sec_finals=$( grep -e '^>' -e '^*' -e '^-' <<< "$bg_input" )

unset bg_input

# > functions
pull_option() {
  if [ $1 != 0 ]
    then sed -n "${1}{ s/$/ /;p; }" <<< "$sec_options"
  fi
}

make_replace_string() {
  sed 's/\//\\\//g;
       s/^/s\/\%/;
       s/\ /\//;
       s/$/\/\;/;' <<< "$sec_replaces"
}

# > MAIN SEQUENCE
#   parse arguments
if [ $# == '0' ]
  then mode="qckset";
  else mode="ftrset";
fi

priority_ftr='>'
option_ftr='[0-9]*?'
tag_ftr='[A-Z][A-Z][A-Z][A-Z]' # cheapestakes, oyy!
safe_ftr='[SN]'
name_ftr='[a-zA-Z][a-zA-Z0-9_]*'

#   if we're in filterset mode go ahead
[ $mode == "ftrset" ] && priority_str='[\-\*>]' && \
while getopts ':rSt:do:lpn:' opt
do
  case $opt in
    'r')  echo "Option -${OPTARG} is disabled for now";
          exit 1;
          ;;
    'S')  safe_ftr='S';
          ;;
    't')  tag_ftr="$OPTARG" ;;
    'o')  option_no="$OPTARG" ;;
    'd')  priority_ftr='[>\*]' ;;
    'n')  name_ftr="([a-zA-Z][a-zA-Z0-9_])*${OPTARG}[a-zA-Z0-9_]*";
          priority_ftr='[\-\*>]'
          ;;
    'l')  mode='list' ;;
    'p')  mode='issafe' ;;
    ':')  echo "Option -${OPTARG} requires an argument";
          exit 1;
          ;;
    \?)   echo "Option -${OPTARG} does not exist";
          exit 1;
          ;;
  esac
done

filter_str="${priority_ftr} ${safe_ftr} ${tag_ftr} ${name_ftr} "
replace_str="$(make_replace_string)"

case "$mode" in
  'qckset' | 'ftrset')
#   TODO: "This room is a bloody pigsty"
    selected_final=$( grep -m1 -P "$filter_str" <<< "$sec_finals")
    entry_option_no=$( cut -d' ' -f5 <<< "$selected_final" )
    image_str=$( cut -d' ' -f6- <<< "$selected_final" )
    
    if [[ "$option_no" ]]
      then option_str=$( pull_option "$option_no" )
      else option_str=$( pull_option "$entry_option_no" )
    fi

    full_str=$( echo "${option_str}${image_str}" | sed "${replace_str}" )
    $(echo "${cmd_root} ${full_str}")
    ;;

  'list')
    ;;

  'issafe')
    [ $(grep -P "$filter_str" <<< "$sec_finals" | cut -d' ' -f2) == 'S' ] && echo "Safe. Bring in your conservative relative." || echo "UnSafe... Chase the kids away!"
    ;;
esac

# DO NOT GO OVER THIS LINE
