# wang

"wang" was originally named "setbg". This small script was and still is made
for usage with `xsetbg` (usually included as a utility for X11).

## Why?

Hey! We're guys, and regardless of our sentiments when we put a 
(half-)nude/swimsuit model as our wallpaper, the conservatives are going to riot
and kids are going to gawk (though recent experiences lead me to believe that
that isn't true). So, in order to counter the fact that CLI programs to change
backgrounds is cumbersome, I made this. Now all I do is use the app runner,
enter the command and voila!

Also changing your `autorun.sh` for every background change is annoying.

## How do I get it to work then?

Simple, really! The main thing here is the background file in the home directory
, aptly named `[.]backgrounds` (note the brackets).

Format is as follows (line numbers on the left for clarity):

    +++ FILE: ~/.backgrounds +++
    01 <ROOT COMMAND>
    02 
    03 R <STRING> <SUBSTITUTE>
    04 R "
    05 R "
    06
    07 O <COMMON OPTION TO SUPPLY TO ROOT COMMAND>
    08 O "
    09 O "
    10
    11 - <SFLAG> <TG> <IDENTIFIER> <CO> <FINAL OPTIONS> 
    12 - <SFLAG> <TG> <IDENTIFIER> <CO> <FINAL OPTIONS> 
    13 - <SFLAG> <TG> <IDENTIFIER> <CO> <FINAL OPTIONS> 
    14 > <SFLAG> <TG> <IDENTIFIER> <CO> <FINAL OPTIONS> 
    15 * <SFLAG> <TG> <IDENTIFIER> <CO> <FINAL OPTIONS> 
    +++ FILE END +++

Line by line explanation:

  1. Line 01 is the "ROOT COMMAND"; technically the command you usually use to
     change the background (xsetbg in this case).
  2. Lines 03-05 are substitutions that are performed before the "ROOT COMMAND"
     is executed with the parsed arguments. Think of it as a `sed s/x/X/g`
     command. Identifiers are to be placed within %'s when used in the next two
     sections (e.g. `%HOT%`).
  3. Lines 07-09 are "COMMON OPTIONS"; options that you pass to the
     "ROOT COMMAND" that you think you might use again and again. Word of
     advice: when adding new ones, add them to the end. Note also that the `0`
     option means no common option is used.

Lines 11-15 require some more explanation. These lines include the actual
background data "wang" uses.

These lines consist of fields:

  1. Is a single character that can be one of
     + `-`, denotes a background that can be used via sending an identifier
       when "wang" is invoked.
     + `>`, denotes a background that is implicitly used whenever "wang" is
       invoked.
     + `*`, denotes a background that can be used when invoking "wang" with the 
       "tag" option.
  2. Is a one-character "SAFETEY FLAG". "wang" uses this to determine if a
     specific background is "unsafe" (think boobies or gore).
     + `S`, denotes a safe background.
     + `N`, denotes an unsafe background.
  3. Is a four-character "TAG" that can be used to categorize sets of these 
     backgrounds.
  4. Is an optional identifier you use to easily change backgrounds with a 
     mnemonic. It can be used with the "any" option.
  5. Is the number of one of the "COMMON OPTIONS" you choose to pass when using
     this specific background. You can override when running "wang" (see below).
  6. Is the background i.e. the image file and/or any other options you wish
     to pass to the "ROOT COMMAND" when you use it.

# Usage

    wang[.sh] [ -r ] [ -S ] [ -t <tag> ] [ -o <number> ] [ -l | -p ] [ -n <identifier> ]
    
    -r  Randomize selection (mutually exclusive with -f)
    -S  Use only safe options
    -t  Select only fron <tag>
    -o  Use this option number instead of the one in the background file
    -d  Use a default from the filtered entries
    -l  Lists all the files that have been filtered
    -p  Print safety information about the top-most final
    -n  Use the entry with this <identifier>
    
    If no arguments or options are given, wang assumes you want to use
    the default final. 

## "wang" ?

`<voice style="person: zefrank1">`

The "wang" is not merely proof that programmers will name their programs 
anything, it's also a discordic-pseudo-acronym. Which means that the name is
supposed to play on the weird fantasies of those that have a dirty mind. 

Very fitting, for "wang" stands for [Weeping Angel]; those freaky angel like
statues in the Doctor Who series. Imagine looking at one, looking away, and
then looking back to see the horrific face of the angel ready to devour you but
this time with the image of a Hooters poster girl replaced with a comforting
landscape when you look back... that is how "wang" do.

For the record, it has nothing to do with the fact that the name is informal for
"penis", because that would be weird, think about it! Now stop thinking about
it you perv... stop thinking about it... why are you still thinking about it?

`</voice>`

[Weeping Angel]: http://tardis.wikia.com/wiki/Weeping_Angel
